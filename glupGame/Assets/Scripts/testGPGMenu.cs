﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AdMobPlugin))]
public class testGPGMenu : MonoBehaviour
{
	
	public GUISkin skin;
	private const string AD_UNIT_ID = "ca-app-pub-2776655795280408/7995276549";
	private const string INTERSTITIAL_ID = "ca-app-pub-2776655795280408/7995276549";
	private AdMobPlugin admob;
	
	public bool hidden = true;
	
	
	public void Awake()
	{
		DontDestroyOnLoad(this);
	}
	
	public void Start()
	{
		admob = GetComponent<AdMobPlugin>();
		//admob.CreateBanner(AD_UNIT_ID, AdMobPlugin.AdSize.SMART_BANNER, true, INTERSTITIAL_ID, true);
		admob.CreateBanner(AD_UNIT_ID, AdMobPlugin.AdSize.SMART_BANNER, false, INTERSTITIAL_ID, false);
		admob.RequestAd();
		admob.HideBanner();
		admob.ShowBanner();
	}
	
	// Update is called once per frame
	public void OnGUI()
	{
		admob.ShowBanner();
	
	}
	
	void OnEnable()
	{
		AdMobPlugin.InterstitialLoaded += HandleInterstitialLoaded;
	}
	
	void OnDisable()
	{
		AdMobPlugin.InterstitialLoaded -= HandleInterstitialLoaded;
	}
	
	void HandleInterstitialLoaded()
	{
		admob.ShowInterstitial();
	}
}