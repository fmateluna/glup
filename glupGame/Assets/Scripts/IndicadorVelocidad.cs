﻿using UnityEngine;
using System.Collections;

public class IndicadorVelocidad : MonoBehaviour {

    public float velocidadMax;
	// Use this for initialization
	void Start () {
        GameEventManager.GameStart += GameStart;
        GameEventManager.GameOver += GameOver;
        gameObject.SetActive(false);
	}

    private void GameOver()
    {
        gameObject.SetActive(false);
    }

    private void GameStart()
    {
        gameObject.SetActive(true);
    }

	// Update is called once per frame
	void Update () {
        if (Glup.velocidadX >= velocidadMax)
        {
            gameObject.guiTexture.enabled = true;
        }
        else {
            gameObject.guiTexture.enabled = false;
        }
	}
}
