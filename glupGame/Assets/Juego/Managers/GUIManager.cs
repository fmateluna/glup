﻿using UnityEngine;
using System;

public class GUIManager : MonoBehaviour
{

    private static GUIManager instance;
    public GUIText boostsText, distanceText, instructionsText, exitText;
    public GUITexture gameOverText, runnerText, panelInfo;  
    private int tiempo;
    private bool gameOver = false;
    public Boolean borrarTodo;
    public AudioClip temaAudio;

    void Start()
    {
        if (borrarTodo)
        {
            PlayerPrefs.DeleteAll();
        }
        gameOver = false;
        instance = this;
        instance.distanceText.text = (PlayerPrefs.GetInt("Maximo Puntaje")).ToString("f0");
        GameEventManager.GameStart += GameStart;
        GameEventManager.GameOver += GameOver;
        gameOverText.enabled = false;
        panelInfo.enabled = false;
    }

    void Update()
    {
        /*
        if (Glup.desinflate)
        {
            this.audio.Stop();
            this.audio.pitch = 1.8f;
            this.audio.Play();
            Debug.Log("[GuiManager] Mas rapido");
        }
        else
        {
            this.audio.Stop();
            this.audio.pitch = 1f;
            this.audio.Play();
            Debug.Log("[GuiManager] Normal");
        }
        */
        if (Input.GetButtonDown("Jump") || (Input.touchCount >= 1 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            GameEventManager.TriggerGameStart();
        }
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }

        if (gameOver)
        {
            tiempo++;
            if (tiempo >= 600)
            {
                distanceText.enabled = false;
                boostsText.enabled = false;
                panelInfo.enabled = false;
                gameOverText.enabled = false;
                runnerText.enabled = true;
                tiempo = 0;
            }

        }
    }

    private void GameStart()
    {
        panelInfo.enabled = true;
        gameOverText.enabled = false;
        instructionsText.enabled = false;
        enabled = false;
        exitText.enabled = false;
        distanceText.enabled = true;
        boostsText.enabled = true;
        runnerText.enabled = false;
        audio.Play();
    }

    private void GameOver()
    {
        audio.Stop();
        int maximo = PlayerPrefs.GetInt("Maximo Puntaje");
        if (!distanceText.text.Equals(maximo.ToString("f0")))
        {
            instructionsText.text = (distanceText.text + "/" + maximo.ToString("f0"));
        }
        else {
            instructionsText.text = maximo.ToString("f0");
        }
        Debug.Log("Ultimo puntaje : " + distanceText.text);
        gameOverText.enabled = true;
        instructionsText.enabled = true;
        exitText.enabled = true;
        enabled = true;
        gameOver = true;
    }

    public static void SetBoosts(int boosts)
    {
        Debug.Log("SetBoosts : " + boosts);
        instance.boostsText.text = boosts.ToString();
    }

    public static bool SetDistance(float distance)
    {
        int puntos = (int)(distance / 10);
        instance.distanceText.text = puntos + "";
        int maximo = PlayerPrefs.GetInt("Maximo Puntaje");
        if (puntos > maximo)
        {
            PlayerPrefs.SetInt("Maximo Puntaje", puntos);
            return true;
        }
        else
        {
            return false;
        }
    }

}