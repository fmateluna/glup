﻿using UnityEngine;
using System.Collections;

public class Flotar : MonoBehaviour
{

		private int altura;
		private bool sube = false;
		// Use this for initialization
		void Start ()
		{
				gameObject.SetActive (true);
				altura = Random.Range (0, 20);
				if (altura > 10) {
						sube = true;
				} else {
						sube = false;
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (sube) {	
						this.transform.Translate (new Vector3 (0, 0.0005f));
						this.transform.Rotate (new Vector3 (0, 0, 0.01f));
				} else {
						this.transform.Translate (new Vector3 (0, -0.0005f));
						this.transform.Rotate (new Vector3 (0, 0, -0.01f));
				}
				if (altura > 20) {
						sube = !sube;	
						altura = 0;
				}
				altura++;
		}
}
