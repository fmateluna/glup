﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    private static int[] niveles;
    private static int[] azar;
    private static int maximoMundo = 10;
    private static int maximoNivel;

    void Start()
    {
        valorNextNivel = 100;
        GameEventManager.GameStart += GameStart;
        GameEventManager.GameOver += GameOver;
        cargarNiveles();
    }
    private void cargarNiveles()
    {
        maximoNivel = (int)(PlayerPrefs.GetInt("Maximo Puntaje") / valorNextNivel)+1;
        niveles = new int[maximoMundo];
        niveles[0] = 0;
        string registroNivel = "0,";
        inicializaAzar();
        int z = 0;
        for (int nivel = 1; nivel < maximoMundo; nivel++)
        {
            if (nivel < maximoNivel)
            {
                niveles[nivel] = azar[z];
                z++;
            }
            else
            {
                niveles[nivel] = nivel;
            }
            registroNivel += niveles[nivel] + ",";
        }
        Debug.Log("Secuencia Niveles :" + registroNivel);
    }

    void inicializaAzar()
    {
        azar = new int[maximoNivel];        
        for (int z = 0; z < azar.Length; z++)
        {            
            int azarNivel = Random.Range(1, maximoNivel);
            if (z == 0)
            {
                azar[z] = azarNivel;
            }
            else
            {                
                bool repetido = true;
                int u = 0;
                for (;;)
                {
                    repetido = false;
                    azarNivel = Random.Range(1, maximoNivel);
                    for (int x = 0; x <= z; x++)
                    {
                        if (azar[x] == azarNivel)
                        {
                            repetido = true;
                        }
                    }
                    if (!repetido)
                    {
                        break;
                    }
                    u++;
                    if (u > 100) {
                        break;
                    }
                }
                azar[z] = azarNivel;
            }
        }
    }

    void Update()
    {

    }

    private void GameStart()
    {
        cargarNiveles();
    }

    private void GameOver()
    {
        //cargarNiveles();
    }

    internal static int obtenerProximoNivel(int mundo)
    {
        return niveles[mundo];
    }

    public static float valorNextNivel { get; set; }
}
