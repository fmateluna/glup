﻿using UnityEngine;

public class Carolo : MonoBehaviour
{

		public Vector3 offset, rotationVelocity;
		public float recycleOffset, spawnChance;
		private bool salta;
		private int altura;
		public Texture caroloNormal, caroloEnojado;
		public bool enojado;
		private int paciencia;
		public AudioClip enojoAudio;		
		void Start ()
		{
				transform.renderer.material.SetTexture ("_MainTex", caroloNormal); 
				GameEventManager.GameOver += GameOver;
				enojado = false;
				gameObject.SetActive (false);
				altura = 0;
				salta = true;
				paciencia = 300;
		}

		void Update ()
		{
				if (enojado) {						
						transform.renderer.material.SetTexture ("_MainTex", caroloEnojado); 
						transform.Translate(Vector3.up * ((300-paciencia)/30) * Time.deltaTime, Space.World);
						paciencia--;
						if (paciencia < 0) {
								paciencia = 300;
								enojado = false;
						}
				} else {
					transform.renderer.material.SetTexture ("_MainTex", caroloNormal); 		
				}
				if (transform.localPosition.x + recycleOffset < Glup.distanceTraveled) {
						gameObject.SetActive (false);
						return;
				}
				if (salta) {
						altura++;
						if (altura == 10) {
								salta = false;
						}
				} else {
						altura--;
						if (altura == 0) {
								salta = true;
						}
				}
		}

		void OnCollisionEnter (Collision other)
		{
				Debug.Log ("ERIZO[OnCollisionEnter] : " + other.gameObject.name);
				if (other.gameObject.name.Equals ("Glup")) {					
					enojado=true;		
					audio.PlayOneShot (enojoAudio, 50F);
				}

		}


		public bool SpawnIfAvailable (Vector3 position)
		{
				if (gameObject.activeSelf || spawnChance <= Random.Range (0f, 6f)) {
						return false;
				}
				transform.localPosition = position + offset;
				gameObject.SetActive (true);
				return true;
		}

		private void GameOver ()
		{
				gameObject.SetActive (false);
		}
}