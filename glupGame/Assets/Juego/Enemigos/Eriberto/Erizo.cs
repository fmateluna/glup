﻿using UnityEngine;
using System.Collections;

public class Erizo : MonoBehaviour
{
    private int altura;
    private bool sube = false;
    public float recycleOffset;
    private bool meTocoGlup;
    public Texture erizo, erizoMareado_A, erizoMareado_B;
    public AudioClip reventacionAudio;
    void Start()
    {
        GameEventManager.GameOver += GameOver;
        meTocoGlup = false;
        altura = Random.Range(0, 80);
        if (altura > 40)
        {
            sube = true;
        }
        else
        {
            sube = false;
        }
        renderer.enabled = true;
        enabled = true;
        gameObject.SetActive(true);

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name.Equals("Glup"))
        {        
            meTocoGlup = true;
            if (!Glup.desinflate)
            {
                audio.PlayOneShot(reventacionAudio, 50F);
                Glup.desinflate = true;
            }
            
        }

    }

    private void GameOver()
    {
        try
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        catch (System.Exception e) { }
    }
    // Update is called once per frame
    void Update()
    {
        //Debug.Log ("[Erizo] Update!!");
        if (meTocoGlup) {
            int pose = Random.Range(0, 100);
            //Debug.Log("pose " + pose);
            if (pose > 90)
                transform.renderer.material.SetTexture("_MainTex", erizoMareado_A);
            else
                transform.renderer.material.SetTexture("_MainTex", erizoMareado_B);            
            rigidbody.isKinematic = false;
            this.transform.Rotate(new Vector3(0, 0, 1f));
        }
        else
        {
            transform.renderer.material.SetTexture("_MainTex", erizo);
            if (sube)
            {
                this.transform.Translate(new Vector3(0, 0.005f));
                this.transform.Rotate(new Vector3(0, 0, 1f));
            }
            else
            {
                this.transform.Translate(new Vector3(0, -0.005f));
                this.transform.Rotate(new Vector3(0, 0, -1f));
            }
            if (altura > 80)
            {
                sube = !sube;
                altura = 0;
            }
            altura++;
            if ((transform.localPosition.x + recycleOffset < Glup.distanceTraveled))
            {
                if (meTocoGlup)
                {
                    Debug.Log("[Erizo] Eliminacion!!");
                    gameObject.SetActive(false);
                    Destroy(gameObject);
                }
                else
                {
                    //Debug.Log ("[Erizo] Mas lejos!!");
                    float rangoPrudente = recycleOffset + 30;
                    float x = transform.localPosition.x + (Random.Range(rangoPrudente, rangoPrudente * 2));
                    float y = Random.Range(10, 20);
                    gameObject.transform.position = new Vector3(x, y, 0f);
                }
            }
            else
            {
                gameObject.SetActive(true);
            }
        }

    }
}
