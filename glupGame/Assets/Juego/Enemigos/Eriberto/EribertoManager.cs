﻿using UnityEngine;
using System.Collections;

public class EribertoManager : MonoBehaviour
{
    public GameObject erizoAdan;
    private GameObject erizo;
    private int proximoNivel;
    public int max;
    public int minY, maxY;
    // Use this for initialization
    void Start()
    {
        GameEventManager.GameStart += GameStart;
        GameEventManager.GameOver += GameOver;
        erizo = (GameObject)Instantiate(erizoAdan, erizoAdan.transform.position, Quaternion.identity);
        proximoNivel = max;
    }

    private void GameStart()
    {
        proximoNivel = max;
        for (int e = 0; e < 1; e++)
        {
            float x = Random.Range(10, 180);
            float y = Random.Range(minY, maxY);
            GameObject eribertoClon = (GameObject)Instantiate(erizo, new Vector3(x, y, 0f), Quaternion.identity);
            eribertoClon.renderer.enabled = true;
            eribertoClon.SetActive(true);
        }
    }

    private void GameOver()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        int nivel = (int)Glup.distanceTraveled / 10;
        //Debug.Log("[ErizoManager] nivel " + nivel + " / proximo nivel " + proximoNivel);
        if (nivel > proximoNivel)
        {
            int masErizos = ((int)(nivel / 100)) + 1;
            for (int e = 0; e < masErizos; e++)
            {
                float x = Random.Range(10, 90);
                float y = Random.Range(minY, maxY);
                GameObject eribertoClon = (GameObject)Instantiate(erizo, new Vector3(x, y, 0f), Quaternion.identity);
                eribertoClon.renderer.enabled = true;
                eribertoClon.SetActive(true);
            }
            Debug.Log("[ErizoManager] MAS ENEMIGOS!! " + masErizos);
            proximoNivel = proximoNivel + max;
        }
    }
}
