﻿using UnityEngine;
using System.Collections;

public class burbuja : MonoBehaviour {
    public Texture pezAzulText, pezRojoText, pezAmarilloText, pezVerdeText, burbujaText;
	// Use this for initialization
    private bool especial;
	void Start () {

        int queEres = Random.Range(0, 4);
        especial = false;
        transform.renderer.material.SetTexture("_MainTex", burbujaText);
        if (queEres == 1)
        {
            transform.renderer.material.SetTexture("_MainTex", pezAzulText);
            especial = true;
        }
        if (queEres == 2)
        {
            transform.renderer.material.SetTexture("_MainTex", pezRojoText);
            especial = true;
        }
        if (queEres == 3)
        {
            transform.renderer.material.SetTexture("_MainTex", pezAmarilloText);
            especial = true;
        }
        if (queEres == 4)
        {
            transform.renderer.material.SetTexture("_MainTex", pezVerdeText);
            especial = true;
        }
    }
	
	// Update is called once per frame
	void Update () {

	
	}

	void OnTriggerEnter (Collider other) {

			gameObject.SetActive (false);

	}
}
