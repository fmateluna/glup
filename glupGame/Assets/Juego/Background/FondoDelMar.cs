﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FondoDelMar : MonoBehaviour
{
    public Transform trenPlayer;
    public GameObject chunkPrefab;

    public float chunkMeshSize;
    public int quadsPerChunk;
    public float height;
    public float heightScale;
    public float xScale;
    public int renderDistance = 5;
    public float colliderWidth = 1;
    public int offset = 0;


    public float xPosicionInicial;
    public float yPosicionInicial;
    public float zPosicionInicial;
    private Queue<GameObject> cerroLista;
    // Use this for initialization
    void Start()
    {
        cerroLista = new Queue<GameObject>();
        InstantiateChunks();
    }

    void Update()
    {
        /*
        if (player.transform.position.x < 0)
        {
            player.position = new Vector3(chunkMeshSize, player.position.y, 0);
            curChunk--;
            InstantiateChunks();
        }*/

        if (trenPlayer.transform.position.x + 180f > xPosicionInicial)
        {

            Debug.Log("ACA DEBO GENERAR MAS CERROS!!");
            newChunks();
        }

    }

    public Mesh GenerateChunkMesh(int chunkX)
    {
        Vector3[] vertices = new Vector3[quadsPerChunk * 4];
        int[] triangles = new int[quadsPerChunk * 6];
        Vector3[] normals = new Vector3[quadsPerChunk * 4];
        Vector2[] uv = new Vector2[quadsPerChunk * 4];

        float tileSize = chunkMeshSize / quadsPerChunk;

        for (int x = 0; x < quadsPerChunk; x++)
        {
            int quadIndex = x * 4;
            int triIndex = x * 6;

            vertices[quadIndex] = new Vector3(x * tileSize, 0);
            vertices[quadIndex + 1] = new Vector3((x + 1) * tileSize, 0);
            vertices[quadIndex + 2] = new Vector3((x + 1) * tileSize, height + heightScale * Mathf.PerlinNoise((float)(x + 1 + quadsPerChunk * chunkX) / quadsPerChunk * xScale + offset, 0));
            vertices[quadIndex + 3] = new Vector3(x * tileSize, height + heightScale * Mathf.PerlinNoise((float)(x + quadsPerChunk * chunkX) / quadsPerChunk * xScale + offset, 0));

            normals[quadIndex] = Vector3.back;
            normals[quadIndex + 1] = Vector3.back;
            normals[quadIndex + 2] = Vector3.back;
            normals[quadIndex + 3] = Vector3.back;

            triangles[triIndex] = quadIndex;
            triangles[triIndex + 1] = quadIndex + 2;
            triangles[triIndex + 2] = quadIndex + 1;

            triangles[triIndex + 3] = quadIndex;
            triangles[triIndex + 4] = quadIndex + 3;
            triangles[triIndex + 5] = quadIndex + 2;

            uv[quadIndex] = new Vector2(0, 0);
            uv[quadIndex + 1] = new Vector2(1, 0);
            uv[quadIndex + 2] = new Vector2(1, 1);
            uv[quadIndex + 3] = new Vector2(0, 1);
        }

        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.normals = normals;
        mesh.uv = uv;

        return mesh;
    }
    public Mesh GenerateMeshCollider(Mesh inputMesh)
    {
        Vector3[] vertices = new Vector3[quadsPerChunk * 4];
        int[] triangles = new int[quadsPerChunk * 40];

        for (int x = 0; x < quadsPerChunk; x++)
        {
            int quadIndex = x * 4;
            int triIndex = x * 6;

            //Vector3 inicial = new Vector3(inputMesh.vertices[quadIndex + 2].x, inputMesh.vertices[quadIndex + 2].y, -colliderWidth / 2);

            vertices[quadIndex] = new Vector3(inputMesh.vertices[quadIndex + 2].x, inputMesh.vertices[quadIndex + 2].y, -colliderWidth / 2);
            vertices[quadIndex + 1] = new Vector3(inputMesh.vertices[quadIndex + 3].x, inputMesh.vertices[quadIndex + 3].y, -colliderWidth / 2);
            vertices[quadIndex + 2] = new Vector3(inputMesh.vertices[quadIndex + 2].x, inputMesh.vertices[quadIndex + 2].y, colliderWidth / 2);
            vertices[quadIndex + 3] = new Vector3(inputMesh.vertices[quadIndex + 3].x, inputMesh.vertices[quadIndex + 3].y, colliderWidth / 2);

            triangles[triIndex] = quadIndex;
            triangles[triIndex + 1] = quadIndex + 1;
            triangles[triIndex + 2] = quadIndex + 2;

            triangles[triIndex + 3] = quadIndex + 1;
            triangles[triIndex + 4] = quadIndex + 3;
            triangles[triIndex + 5] = quadIndex + 2;
        }

        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        return mesh;
    }


    void newChunks()
    {
        float ultimaPosicionChuck = 0f;
        Mesh chunkMesh = null;
        cerroLista.Clear();
        for (int x = 0; x < renderDistance * 2 + 1; x++)
        {
            int chunkWorldPos = x - renderDistance;
            GameObject chunk = (GameObject)Instantiate(chunkPrefab, new Vector3(xPosicionInicial + (chunkMeshSize * chunkWorldPos), yPosicionInicial, zPosicionInicial), Quaternion.identity);
            chunk.transform.Translate(-20, 0, 0);
            ultimaPosicionChuck = xPosicionInicial + (chunkMeshSize * chunkWorldPos);
            if (x == 0)
            {
                Debug.Log("Continuando ultimo cerro");
                chunkMesh = GenerateChunkMesh(renderDistance * 2);
            }
            else
            {
                chunkMesh = GenerateChunkMesh(x);
            }
            chunk.GetComponent<MeshFilter>().mesh = chunkMesh;
            chunk.GetComponent<MeshCollider>().sharedMesh = GenerateMeshCollider(chunkMesh);
            cerroLista.Enqueue(chunk);
            ultimoX = (x);
        }
        xPosicionInicial += (renderDistance * 2 + 1) * chunkMeshSize;
    }

    void InstantiateChunks()
    {
        if (cerroLista.Count != 0)
        {
            foreach (GameObject chunk in cerroLista)
                Destroy(chunk);

            cerroLista = new Queue<GameObject>();
        }
        float ultimaPosicionChuck = 0f;
        Mesh chunkMesh = null;
        for (int x = 0; x < renderDistance * 2 + 1; x++)
        {
            int chunkWorldPos = x - renderDistance;
            GameObject chunk = (GameObject)Instantiate(chunkPrefab, new Vector3(xPosicionInicial + (chunkMeshSize * chunkWorldPos), yPosicionInicial, zPosicionInicial), Quaternion.identity);
            ultimaPosicionChuck = xPosicionInicial + (chunkMeshSize * chunkWorldPos);
            chunkMesh = GenerateChunkMesh(x);
            chunk.GetComponent<MeshFilter>().mesh = chunkMesh;
            chunk.GetComponent<MeshCollider>().sharedMesh = GenerateMeshCollider(chunkMesh);
            cerroLista.Enqueue(chunk);
        }
        xPosicionInicial = (renderDistance * 2 + 1) * chunkMeshSize;

    }


    public int ultimoX { get; set; }
}
