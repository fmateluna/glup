﻿using UnityEngine;
using System.Collections;

public class Fondo : MonoBehaviour
{
		public Texture[] fondos;
		public int numberOfObjects;
		public static bool gameStar = false;
        private int altura;
        private bool sube = false;
		// Use this for initialization
		void Start ()
		{
				int maximo = PlayerPrefs.GetInt ("Maximo Puntaje");	
				int qFondo = (int)(maximo / 100);
				if (qFondo > 9) {
						qFondo = 9;
				}
				transform.renderer.material.SetTexture ("_MainTex", fondos [qFondo]);
                gameObject.SetActive(true);
                altura = Random.Range(0, 20);
                if (altura > 10)
                {
                    sube = true;
                }
                else
                {
                    sube = false;
                }
		}

		private void GameStar ()
		{
				gameStar = true;
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (gameStar) {
						float nivel = Glup.distanceTraveled / GameManager.valorNextNivel;
						int qFondo = (int)(nivel / 10);
						if (qFondo > 9) {
								qFondo = 9;
						}
                        transform.renderer.material.SetTexture("_MainTex", fondos[GameManager.obtenerProximoNivel(qFondo)]); 
				}
                if (sube)
                {
                    this.transform.Translate(new Vector3(0, 0.0005f));
                    this.transform.Rotate(new Vector3(0, 0, 0.01f));
                }
                else
                {
                    this.transform.Translate(new Vector3(0, -0.0005f));
                    this.transform.Rotate(new Vector3(0, 0, -0.01f));
                }
                if (altura > 20)
                {
                    sube = !sube;
                    altura = 0;
                }
                altura++;
		}
}
