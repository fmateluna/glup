﻿using UnityEngine;
using System.Collections.Generic;

public class SkylineManager : MonoBehaviour
{

		public Transform prefab;
        public int cambioNivel;
		public int numberOfObjects;
		public float recycleOffset;
		public Vector3 startPosition;
		public Vector3 minSize, maxSize;
		public GUIText puntajeText;
		private Vector3 nextPosition;
		private Queue<Transform> objectQueue;
		public Texture[] mundos;
        public int horizonteMinimo;

		void Start ()
		{
				GameEventManager.GameStart += GameStart;
				GameEventManager.GameOver += GameOver;
				objectQueue = new Queue<Transform> (numberOfObjects);
				for (int i = 0; i < numberOfObjects; i++) {
						int rotacion = Random.Range (-1, 1);
						prefab.Rotate (0, 0, rotacion);
						Transform elementoFondo = (Transform)Instantiate (prefab, new Vector3 (0f, 0f, -100f), Quaternion.identity);
                        elementoFondo.renderer.material.SetTexture("_MainTex", mundos[0]); 
						objectQueue.Enqueue (elementoFondo);
				}
				enabled = false;

		}

		void Update ()
		{				
				if (objectQueue.Peek ().localPosition.x + recycleOffset < Glup.distanceTraveled) {
					Recycle (Glup.distanceTraveled/10);
				}
		}

		private void Recycle (float nivel)
		{
				Vector3 scale = new Vector3 (
			Random.Range (minSize.x, maxSize.x),
			Random.Range (minSize.y, maxSize.y),
			Random.Range (minSize.z, maxSize.z));

				Vector3 position = nextPosition;
				position.x += (scale.x * 0.5f);
				position.y += (scale.y * 0.5f) + 25;

				Transform fondoEncolado = objectQueue.Dequeue ();
				fondoEncolado.localScale = scale;
				fondoEncolado.localPosition = position;
				nextPosition.x += scale.x;
                nextPosition.z = horizonteMinimo + Random.Range(0, 50);				
				//Debug.Log ("########## NIVEL " + nivel);
                int mundo = (int)(nivel / GameManager.valorNextNivel);
				//Debug.Log ("Nivel:" + mundo);
                if (mundo > 9)
                {
                    mundo = Random.Range(0, 8);
                }
                fondoEncolado.transform.renderer.material.SetTexture("_MainTex", mundos[GameManager.obtenerProximoNivel(mundo)]); 
				objectQueue.Enqueue (fondoEncolado);
		}
	
		private void GameStart ()
		{
				Glup.distanceTraveled = 0;
				nextPosition = startPosition;
				for (int i = 0; i < numberOfObjects; i++) {
						Recycle (0f);
				}
				enabled = true;
		}

		private void GameOver ()
		{
				/*
				for (int i = 0; i < numberOfObjects; i++) {
					Recycle (0f);
				}
				*/
				enabled = false;
		}
}