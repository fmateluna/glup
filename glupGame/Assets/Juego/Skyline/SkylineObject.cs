﻿using UnityEngine;
using System.Collections;

public class SkylineObject : MonoBehaviour {

    private int altura;
    private bool sube = false;
    // Use this for initialization
    void Start()
    {
        gameObject.SetActive(true);
        altura = Random.Range(0, 20);
        if (altura > 10)
        {
            sube = true;
        }
        else
        {
            sube = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (sube)
        {
            this.transform.Rotate(new Vector3(0, 0, 0.01f));
        }
        else
        {
            this.transform.Rotate(new Vector3(0, 0, -0.01f));
        }
        if (altura > 30)
        {
            sube = !sube;
            altura = 0;
        }
        altura++;
    }
}
