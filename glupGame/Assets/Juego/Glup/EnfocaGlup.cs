﻿using UnityEngine;
using System.Collections;

public class EnfocaGlup : MonoBehaviour {
	public GameObject player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float newX =  player.transform.position.x;
		float newZ =  player.transform.position.z;
		float y = transform.position.y;
		transform.position = new Vector3(newX, y, newZ);   
	}
}
