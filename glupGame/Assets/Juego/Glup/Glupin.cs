﻿using UnityEngine;
using System.Collections;

public class Glupin : MonoBehaviour
{
    public static float distanceTraveled;
    private static int boosts;
    public float acceleration;
    public Vector3 jumpVelocity, superJumpVelocity;
    public float gameOverY;
    public float gameOverPreY;
    private bool tocoOtroObjeto;
    private Vector3 startPosition;
    public Vector3 offset, rotationVelocity;
    public Texture nadandoJumpA, nadandoJumpB;
    public AudioClip jumpAudio;
    public AudioClip gameOverAudio;
    public AudioClip gameOverAudioFatality;
    //private int jumpy = 0;

    void Start()
    {
        desinflate = false;
        GameEventManager.GameStart += GameStart;

        try
        {
            startPosition = transform.localPosition;
        }
        catch (UnityException e)
        {
            Debug.LogError("[GameStart]Error " + e.Message);
        }
        renderer.enabled = false;
        rigidbody.isKinematic = true;
        enabled = false;
    }

    private void GameStart()
    {
        boosts = 0;
        desinflate = false;
        Fondo.gameStar = true;
        GUIManager.SetBoosts(boosts);
        distanceTraveled = 0f;
        GUIManager.SetDistance(distanceTraveled);
        transform.localPosition = startPosition;
        renderer.enabled = true;
        rigidbody.isKinematic = false;
        try
        {
            transform.localPosition = startPosition;
        }
        catch (UnityException e)
        {
            Debug.LogError("[GameStart]Error " + e.Message);
        }
        enabled = true;
        //this.audio.Play ();
    }

    IEnumerator momento(float duration)
    {
        //This is a coroutine
        Debug.Log("Start momento() function. The time is: " + Time.time);
        Debug.Log("Float duration = " + duration);
        yield return new WaitForSeconds(duration);   //Wait
        Debug.Log("End momento() function and the time is: " + Time.time);
    }

    void Update()
    {

        if (Input.GetButtonDown("Jump") || (Input.touchCount >= 1 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            StartCoroutine(momento(3.5f)); 
            Debug.Log("[Glupin!!] Salta");
            audio.PlayOneShot(jumpAudio, 50F);
            rigidbody.AddForce(jumpVelocity, ForceMode.VelocityChange);
        }

        transform.renderer.material.SetTexture("_MainTex", ((int)Random.Range(0, 5))>2 ? nadandoJumpA : nadandoJumpB);
        if ((transform.localPosition.x + 40 < Glup.distanceTraveled))
        {
            audio.PlayOneShot(gameOverAudio, 50F);
            float rangoPrudente = 30;
            float x = transform.localPosition.x + (Random.Range(rangoPrudente, rangoPrudente * 2));
            float y = Random.Range(10, 20);
            gameObject.transform.position = new Vector3(x, y, 0f);
        }

    }

    void FixedUpdate()
    {
        if (tocoOtroObjeto)
        {
            rigidbody.AddForce(acceleration, 0f, 0f, ForceMode.Acceleration);
        }
    }

    void OnCollisionEnter()
    {
        tocoOtroObjeto = true;
    }

    void OnCollisionExit()
    {
        tocoOtroObjeto = false;
    }



    

    public static void AddBoost()
    {
        boosts += 1;
        //Cologar sonido
        desinflate = false;
        Debug.Log("Desinflate False!!");
        Debug.Log("AddBoost :" + boosts);
        GUIManager.SetBoosts(boosts);
    }

    public static void RemoveBoost()
    {
        if (boosts > 1)
        {
            boosts -= 1;
            //Cologar sonido
            GUIManager.SetBoosts(boosts);
        }
        else
        {
            //GameOver
        }
    }



    public static bool desinflate { get; set; }
}