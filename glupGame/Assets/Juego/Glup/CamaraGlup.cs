﻿using UnityEngine;
using System.Collections;

public class CamaraGlup : MonoBehaviour {

	// Use this for initialization
    private float enfoque;
    private float vision = 25f;
    public float enfocaEnano = 22f;
    public float enfocaNormal = 25f;
    void Start()
    {
        enfoque = enfocaNormal;
        GameEventManager.GameStart += GameStart;
        GameEventManager.GameOver += GameOver;
	}
    private void GameStart()
    {
        enfoque = enfocaNormal;
        vision = enfocaNormal;
        camera.fieldOfView = enfocaNormal;
    }
    private void GameOver()
    {
        enfoque = enfocaNormal;
        vision = enfocaNormal;
        camera.fieldOfView = enfocaNormal;
    }
	
	// Update is called once per frame
	void Update () {
        if (enfoque < vision)
        {
            enfoque += 0.08f;
        }
        if (enfoque > vision)
        {
            enfoque -= 0.04f;
        }
        //camera.fieldOfView = enfoque;
        if (Glup.desinflate)
        {
            vision = enfocaEnano;
        }
        else
        {
            vision = enfocaNormal;
        }
	}

    
}
