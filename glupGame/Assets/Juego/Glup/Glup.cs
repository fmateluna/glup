﻿using UnityEngine;

public class Glup : MonoBehaviour
{
    public static float distanceTraveled;
    private static int boosts;
    public float acceleration;
    public Vector3 jumpVelocity, superJumpVelocity;
    public float gameOverY;
    public float gameOverPreY;
    private bool tocoOtroObjeto;
    private Vector3 startPosition;
    private int vertigo = 0;
    private bool yaPasoMaximo;
    public Vector3 offset, rotationVelocity;
    public Texture nadandoJumpA, nadandoJumpB, superJumpGlup, gameOverA, gameOverB, gameOverC, jumpGlupA, jumpGlupB, glupChicoA, glupChicoB;
    private int frame, tiempo;
    public AudioClip jumpAudio;
    public AudioClip gameOverAudio;
    public AudioClip gameOverAudioFatality;
    public static float velocidadX;
    //private int jumpy = 0;

    void Start()
    {
        desinflate = false;
        GameEventManager.GameStart += GameStart;
        GameEventManager.GameOver += GameOver;
        try
        {
            startPosition = transform.localPosition;
        }
        catch (UnityException e)
        {
            Debug.LogError("[GameStart]Error " + e.Message);
        }
        renderer.enabled = false;
        yaPasoMaximo = false;
        frame = 0;
        tiempo = 0;
        rigidbody.isKinematic = true;
        enabled = false;
        transform.renderer.material.SetTexture("_MainTex", superJumpGlup);
    }

    private void GameStart()
    {
        boosts = 0;
        desinflate = false;
        transform.renderer.material.SetTexture("_MainTex", superJumpGlup);
        Fondo.gameStar = true;
        GUIManager.SetBoosts(boosts);
        distanceTraveled = 0f;
        GUIManager.SetDistance(distanceTraveled);
        transform.localPosition = startPosition;
        renderer.enabled = true;
        rigidbody.isKinematic = false;
        try
        {
            transform.localPosition = startPosition;
        }
        catch (UnityException e)
        {
            Debug.LogError("[GameStart]Error " + e.Message);
        }
        enabled = true;
        //this.audio.Play ();
    }

    void Update()
    {
        bool maximaVelocidad = false;
        bool gameOver = false;
        //rigidbody.mass = transform.GetChildCount()*50;
        //Debug.Log("Cantidad hijos " + transform.GetChildCount());        

        velocidad = rigidbody.velocity;
        velocidadX = velocidad.x;
        if (velocidad.x > 30)
        {
            Debug.Log("Muy Rapido!! Velocidad : " + rigidbody.velocity);
            velocidad.Set(30, velocidad.y, velocidad.z);
            rigidbody.velocity = velocidad;
            maximaVelocidad = true;
        }
        else
        {
            //Debug.Log("Velocidad : " + rigidbody.velocity);
        }

        if (desinflate)
        {
            this.audio.pitch = 1.8f;
        }
        else
        {
            this.audio.pitch = 1f;
        }

        if (Input.GetButtonDown("Jump") || (Input.touchCount >= 1 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            bool salto = false;
            if (frame == 0)
            {
                transform.renderer.material.SetTexture("_MainTex", nadandoJumpA);
                frame = 1;
            }
            else
            {
                transform.renderer.material.SetTexture("_MainTex", nadandoJumpB);
                frame = 0;
            }

            if (tocoOtroObjeto)
            {
                int binario = Random.Range(0, 2);
                transform.renderer.material.SetTexture("_MainTex", binario == 0 ? jumpGlupA : jumpGlupB);
                audio.PlayOneShot(jumpAudio, 50F);
                rigidbody.AddForce(jumpVelocity, ForceMode.VelocityChange);
                tocoOtroObjeto = false;
                salto = true;
            }
            /*
            if (desinflate)
            {
                audio.PlayOneShot(jumpAudio, 50F);
                if (jumpy > 3)
                {
                    rigidbody.AddForce(jumpVelocity, ForceMode.VelocityChange);
                    jumpy = 0;
                }
                jumpy++;
                touchingPlatform = false;
            }
             */
            if (boosts > 0 && !salto)
            {
                transform.renderer.material.SetTexture("_MainTex", superJumpGlup);
                rigidbody.AddForce(superJumpVelocity, ForceMode.VelocityChange);
                boosts -= 1;
                GUIManager.SetBoosts(boosts);
                //desinflate = false;
                audio.PlayOneShot(jumpAudio, 50F);
            }

            tiempo = tiempo - 15;
        }
        
        if (maximaVelocidad)
        {
            transform.renderer.material.SetTexture("_MainTex", superJumpGlup);
        }

        if (transform.localPosition.y < gameOverPreY)
        {
            if (transform.localPosition.y < gameOverY + 2)
            {
                transform.renderer.material.SetTexture("_MainTex", gameOverC);
                gameOver = true;
            }
            else
            {
                if (frame == 0)
                {
                    if (tiempo > 15)
                    {
                        transform.renderer.material.SetTexture("_MainTex", gameOverA);
                        tiempo = 0;
                        frame = 1;
                    }
                }
                else
                {
                    if (tiempo > 10)
                    {
                        transform.renderer.material.SetTexture("_MainTex", gameOverB);
                        tiempo = 0;
                        frame = 0;
                    }
                }
                tiempo++;
            }
            if (transform.localPosition.y < gameOverY)
            {
                audio.PlayOneShot(gameOverAudio, 50F);
                //Debug.Log ("position Y " + transform.localPosition.y);
                transform.renderer.material.SetTexture("_MainTex", nadandoJumpA);
                GameEventManager.TriggerGameOver();
            }

        }

        else
        {
            if (frame == 0)
            {
                if (tiempo == 15)
                {
                    transform.renderer.material.SetTexture("_MainTex", nadandoJumpA);
                    tiempo = 0;
                    frame = 1;
                }
            }
            else
            {
                if (tiempo == 30)
                {
                    transform.renderer.material.SetTexture("_MainTex", nadandoJumpB);
                    tiempo = 0;
                    frame = 0;
                }
            }
            tiempo++;
        }

        distanceTraveled = transform.localPosition.x;
        vertigo += 1;
        bool pasoMaximo = GUIManager.SetDistance(distanceTraveled);
        if (pasoMaximo && !yaPasoMaximo)
        {
            boosts += 1;
            GUIManager.SetBoosts(1);
            //Debug.Log("ATENCIOON!!!!");
            yaPasoMaximo = true;
        }
        //Debug.Log("[Glup] Desinflate:" + Glup.desinflate);
        if (!gameOver && Glup.desinflate)
        {
            int binario = Random.Range(0, 2);
            transform.renderer.material.SetTexture("_MainTex", binario == 0 ? glupChicoA : glupChicoB);
        }
    }

    void FixedUpdate()
    {
        if (tocoOtroObjeto)
        {
            rigidbody.AddForce(acceleration, 0f, 3f, ForceMode.Acceleration);
        }
    }

    void OnCollisionEnter()
    {
        tocoOtroObjeto = true;
    }

    void OnCollisionExit()
    {
        tocoOtroObjeto = false;
    }

    void OnCollisionEnter(Collision other)
    {
        string quien = other.gameObject.name;
        Debug.Log("[Glup] quien me toco " + quien);
        if (quien.Equals("Eriberto"))
        {
            desinflate = true;
        }
        tocoOtroObjeto = true;
    }

    private void GameOver()
    {
        this.audio.Stop();
        int fatality = Random.Range(0, 9);
        if (desinflate)
        {
            audio.PlayOneShot(gameOverAudioFatality, 90F);
        }
        else
        {
            audio.PlayOneShot(fatality != 1 ? gameOverAudio : gameOverAudioFatality, 90F);
        }
        renderer.enabled = false;
        rigidbody.isKinematic = true;
        enabled = false;
    }

    public static void AddBoost()
    {
        boosts += 1;
        //Cologar sonido
        desinflate = false;
        Debug.Log("Desinflate False!!");
        Debug.Log("AddBoost :" + boosts);
        GUIManager.SetBoosts(boosts);
    }

    public static void RemoveBoost()
    {
        if (boosts > 1)
        {
            boosts -= 1;
            //Cologar sonido
            GUIManager.SetBoosts(boosts);
        }
        else
        {
            //GameOver
        }
    }



    public static bool desinflate { get; set; }

    public Vector3 velocidad { get; set; }
}