﻿using UnityEngine;
using System.Collections;

public class Booster : MonoBehaviour
{

    public Vector3 offset, rotationVelocity;
    public float recycleOffset, spawnChance;
    public Texture shuriukenA, shuriukenB, shuriukenC, shuriukenMasuno, upTex;
    public AudioClip addBoosterAudio;
    private int pose;
    void Start()
    {
        transform.renderer.material.SetTexture("_MainTex", shuriukenA);
        GameEventManager.GameOver += GameOver;
        gameObject.SetActive(false);
        pose = 1;
    }

    void Update()
    {
        if (pose != 0)
        {
            pose = Random.Range(1, 8);
        }
        //Debug.Log("pose " + pose);
        if (pose == 1)
            transform.renderer.material.SetTexture("_MainTex", shuriukenA);
        if (pose == 2)
            transform.renderer.material.SetTexture("_MainTex", shuriukenB);
        if (pose == 3)
            transform.renderer.material.SetTexture("_MainTex", shuriukenC);

        if ((transform.localPosition.x + recycleOffset < Glup.distanceTraveled))
        {
            gameObject.SetActive(false);
            return;
        }
        else
        {
            gameObject.SetActive(true);
        }
    }


    IEnumerator AntesDeMorir(float duration)
    {
        //This is a coroutine
        Debug.Log("Start AntesDeMorir() function. The time is: " + Time.time);
        Debug.Log("Float duration = " + duration);
        yield return new WaitForSeconds(duration);   //Wait
        Debug.Log("End AntesDeMorir() function and the time is: " + Time.time);
        gameObject.SetActive(false);
        transform.renderer.material.SetTexture("_MainTex", shuriukenA);
        pose = 1;
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("OnTriggerEnter : " + other.gameObject.name);
        if (other.gameObject.name.Equals("Glup"))
        {
            audio.PlayOneShot(addBoosterAudio);
            if (Glup.desinflate)
            {
                Glup.desinflate = false;
                transform.renderer.material.SetTexture("_MainTex", upTex);
                pose = 0;
                StartCoroutine(AntesDeMorir(0.8f));                
            }
            else
            {
                transform.renderer.material.SetTexture("_MainTex", shuriukenMasuno);
                pose = 0;
                StartCoroutine(AntesDeMorir(0.8f));
                Glup.AddBoost();
                
            }
            Debug.Log("OnTriggerEnter [FIN] ");
        }
    }

    public void SpawnIfAvailable(Vector3 position)
    {

        if (gameObject.activeSelf || spawnChance <= Random.Range(0f, 10f))
        {
            return;
        }

        transform.localPosition = position + offset;
        gameObject.SetActive(true);
    }

    private void GameOver()
    {
        gameObject.SetActive(false);
    }
}