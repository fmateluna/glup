﻿using UnityEngine;
using System.Collections.Generic;

public class PlatformManager : MonoBehaviour
{

		public Transform prefab;
		public int numberOfObjects;
		public float recycleOffset;
		public Vector3 startPosition;
		public Vector3 minSize, maxSize, minGap, maxGap;
		public float minY, maxY;
		public Material[] materials;
		public PhysicMaterial[] physicMaterials;
		public Booster booster;
		public Carolo erizo;
		private Vector3 nextPosition;
		private Queue<Transform> objectQueue;
		//public AudioClip caracolCaroloEnojado;
		void Start ()
		{
				GameEventManager.GameStart += GameStart;
				GameEventManager.GameOver += GameOver;
				objectQueue = new Queue<Transform> (numberOfObjects);
				int rotacion = Random.Range (-8, 8);
				for (int i = 0; i < numberOfObjects; i++) {
						Transform plataforma = (Transform)Instantiate (prefab, new Vector3 (0f, 0f, -100f), Quaternion.identity);

						rotacion = Random.Range (rotacion, 5);
						plataforma.Rotate (1, 1, rotacion);
						objectQueue.Enqueue (plataforma);
				}
				//enabled = false;
		}

		void Update ()
		{
				if ((objectQueue.Peek ().localPosition.x + recycleOffset) <= (Glup.distanceTraveled)) {
						Recycle ();
				}
		}

		private void Recycle ()
		{
				try {
						Vector3 scale = new Vector3 (
            Random.Range (minSize.x, maxSize.x),
            Random.Range (minSize.y, maxSize.y),
            Random.Range (minSize.z, maxSize.z));

						Vector3 position = nextPosition;
						position.x += scale.x * 0.5f;
						position.y += scale.y * 0.5f;
						bool siono = Random.Range (0, 6) == 5 ? true : false;
						if (siono) {
								booster.SpawnIfAvailable (position);
						}			
						float nivel = Glup.distanceTraveled/10;
						int probabilidad = (int)(nivel / 100);
						if(probabilidad>=10){
							siono=true;
						}else{
							siono = Random.Range (0, 12-(probabilidad)) == 1 ? true : false;
						}
						//Debug.Log("[Recycle] Probabildiad:"+probabilidad+" Crea Carolo :" + siono );
						if (siono) {
							erizo.SpawnIfAvailable (position);
						}
						Transform o = objectQueue.Dequeue ();
						o.localScale = scale;
						o.localPosition = position;
						int materialIndex = Random.Range (0, materials.Length);
						o.renderer.material = materials [materialIndex];
						o.collider.material = physicMaterials [materialIndex];
						objectQueue.Enqueue (o);

						nextPosition += new Vector3 (
            Random.Range (minGap.x + 2, maxGap.x) + scale.x,
            Random.Range (minGap.y, maxGap.y),
            Random.Range (minGap.z, maxGap.z));

						if (nextPosition.y < minY) {
								nextPosition.y = minY + maxGap.y;
						} else if (nextPosition.y > maxY) {
								nextPosition.y = (maxY - maxGap.y);// + (Runner.distanceTraveled);
						}
				} catch (UnityException ue) {
						Recycle ();
				}

		}

		private void GameStart ()
		{
				nextPosition = startPosition;
				for (int i = 0; i < numberOfObjects; i++) {
						Recycle ();
				}
				enabled = true;
		}

		private void GameOver ()
		{
				enabled = false;
		}
}