﻿using UnityEngine;
using System.Collections;

public class Plataforma : MonoBehaviour
{	
		private int altura;
		private bool sube = false;

		// Use this for initialization
		void Start ()
		{
				gameObject.SetActive (true);
				altura = Random.Range (0, 80);
				if (altura > 40) {
						sube = true;
				} else {
						sube = false;
				}

		}
	
		// Update is called once per frame
		void Update ()
		{

				if (sube) {	
						this.transform.Translate (new Vector3 (0, 0.005f));
						this.transform.Rotate (new Vector3 (0, 0, 0.01f));
				} else {
						this.transform.Translate (new Vector3 (0, -0.005f));
						this.transform.Rotate (new Vector3 (0, 0, -0.01f));
				}
				if (altura > 80) {
						sube = !sube;	
						altura = 0;
				}
				altura++;
		}

		void OnTriggerEnter (Collider other)
		{
				Debug.Log ("Plataforma OnTriggerEnter : " + other.gameObject.name);
				if (other.gameObject.name.Equals ("Glup")) {
						gameObject.SetActive (false);
				}
		}
}
